<?php

namespace Test;

use Exception;

use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\ConsoleOutput;

use Behat\Behat\ApplicationFactory;

use PHPUnit_Framework_TestCase;

class BehatTest extends PHPUnit_Framework_TestCase
{
    /**
     * Tests that Behat scenarii meet acceptance criteria
     */
    public function testThatBehatScenariiMeetAcceptanceCriteria()
    {
        try {
            $input = new ArrayInput(array('-v' => ''));
            $output = new ConsoleOutput();
            $factory = new ApplicationFactory();
            $app = $factory->createApplication();
            $app->setAutoExit(false);
            $result = $app->run($input, $output);
            $this->assertEquals(0, $result);
        } catch (Exception $exception) {
            $this->fail($exception->getMessage());
        }
    }
}
