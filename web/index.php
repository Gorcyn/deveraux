<?php

require_once __DIR__.'/../vendor/autoload.php';

use Gorcyn\Deveraux\Application;

$app = new Application();
$app['debug'] = true;
$app->run();
