<?php

use Symfony\Component\HttpKernel\Client;

use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;

use PHPUnit_Framework_Assert as Assert;

use Gorcyn\Deveraux\Application;

/**
 * Features context.
 */
class FeatureContext implements SnippetAcceptingContext
{
    /**
     * @var Application
     */
    protected $app;

    /**
     * @var Client
     */
    protected $client;

    /**
     * @BeforeScenario
     */
    public function setup($event)
    {
        $app = new Application();
        $app['debug'] = true;
        unset($app['exception_handler']);
        $this->app = $app;
        $this->client = new Client($this->app);
    }

    /**
     * @When /^call "([^"]*)" "([^"]*)"$/
     */
    public function call($method, $endpoint)
    {
        $this->client->request($method, $endpoint);
    }

    /**
     * @When /^call "([^"]*)" "([^"]*)" with parameters:$/
     */
    public function callWithParameters($method, $endpoint, PyStringNode $postParametersStringNode)
    {
        $postParameters = json_decode($postParametersStringNode->getRaw(), true);
        $this->client->request($method, $endpoint, $postParameters);
    }

    /**
     * @When /^call "([^"]*)" "([^"]*)" with headers:$/
     */
    public function callWithHeaders($method, $endpoint, TableNode $headers)
    {
        $tableHeaders = $headers->getTable();
        $headers = array();
        foreach ($tableHeaders as $header) {
            $name = $header[0];
            $headers["HTTP_$name"] = $header[1];
        }
        $this->client->request($method, "{$endpoint}", array(), array(), $headers);
    }

    /**
     * @Then /^response status is "([^"]*)"$/
     */
    public function responseStatusIs($statusCode)
    {
        Assert::assertEquals($statusCode, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @Then /^response status should be "([^"]*)"$/
     */
    public function responseStatusShouldBe($statusCode)
    {
        return $this->responseStatusIs($statusCode);
    }

    /**
     * @Given /^response headers are:$/
     */
    public function responseHeadersAre(TableNode $headers)
    {
        $responseHeaders = $this->client->getResponse()->headers;
        foreach ($headers->getTable() as $header) {
            $name = $header[0];
            $value = $header[1];
            Assert::assertTrue($responseHeaders->has($name));
            Assert::assertEquals($responseHeaders->get($name), $value);
        }
    }

    /**
     * @Given /^response content is blank$/
     */
    public function responseContentIsBlank()
    {
        Assert::assertEmpty($this->client->getResponse()->getContent());
    }

    /**
     * @Given /^json response should be:$/
     */
    public function jsonResponseShouldBe(PyStringNode $expectedResponseStringNode)
    {
        $clientResponse = json_decode($this->client->getResponse()->getContent(), true);
        $expectedResponse = json_decode($expectedResponseStringNode->getRaw(), true);
        Assert::assertEquals($expectedResponse, $clientResponse);
    }

    /**
     * @Given /^html response should be:$/
     */
    public function responseContentIs(PyStringNode $expectedResponseStringNode)
    {
        $content = $expectedResponseStringNode->getRaw();
        Assert::assertEquals($content, $this->client->getResponse()->getContent());
    }
}
