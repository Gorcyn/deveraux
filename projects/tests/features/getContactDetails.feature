Feature: Consume getContactDetails request
    In order to check expected response is given

    Scenario: Consume getContactDetails request and get contact details' json
        When call "GET" "/tests/contacts/1" with headers:
            | User-Token    | THISISARANDOMLYGENERATEDUSERTOKEN |
        Then response status is "200"
        And response headers are:
            | Content-Language | en |
        And json response should be:
        """
        {
            "user": {
                "id": 1,
                "email": "john@doe.com",
                "userName": "JoDoe",
                "firstName": "John",
                "lastName": "Doe",
                "phone": "0102030405"
            }
        }
        """
