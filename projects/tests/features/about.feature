Feature: Consume about request
    In order to check expected response is given

    Scenario: Consume about request and get author's html page
        When call "GET" "/tests/about?about=author"
        Then response status is "200"
        And response headers are:
            | Content-Language | en |
        And html response should be:
        """
        <!DOCTYPE html>
        <html>
            <head>
                <title>About Author</title>
            </head>
            <body>
                <h1>Ludovic Garcia</h1>
                <h2>gorcyn@gmail.com</h2>
            </body>
        </html>
        """
