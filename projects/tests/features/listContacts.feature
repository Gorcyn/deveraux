Feature: Consume listContacts request
    In order to check expected response is given

    Scenario: Consume listContacts request and get contact list's json
        When call "GET" "/tests/contacts/" with headers:
            | User-Token    | THISISARANDOMLYGENERATEDUSERTOKEN |
        Then response status is "200"
        And response headers are:
            | Content-Language | en |
        And json response should be:
        """
        {
            "contacts": [
                {
                    "id": 1,
                    "firstName": "John",
                    "lastName": "Doe"
                },
                {
                    "id": 2,
                    "firstName": "Jane",
                    "lastName": "Doe"
                }
            ]
        }
        """
