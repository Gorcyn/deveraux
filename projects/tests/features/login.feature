Feature: Consume login request
    In order to check expected response is given

    Scenario: Consume login request and get user's json
        When call "POST" "/tests/user/login"
        """
        {
            "login": "gorcyn@gmail.com",
            "password": "pWd"
        }
        """
        Then response status is "200"
        And response headers are:
            | Content-Language | en                                |
            | User-Token       | THISISARANDOMLYGENERATEDUSERTOKEN |
        And json response should be:
        """
        {
            "user": {
                "email": "gorcyn@gmail.com",
                "userName": "Gorcyn",
                "firstName": "Ludovic",
                "lastName": "Garcia"
            }
        }
        """
