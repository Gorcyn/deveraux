<?php

namespace Gorcyn\Deveraux;

class Response extends Message
{
    private $code;

    /**
     * Gets response code
     *
     * @return string The response code
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Sets response code
     *
     * @param  string The response code
     *
     * @return Response The response
     */
    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }
}
