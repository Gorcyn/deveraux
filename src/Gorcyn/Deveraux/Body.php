<?php

namespace Gorcyn\Deveraux;

class Body
{
    const TXT = 'txt';
    const JSON = 'json';
    const HTML = 'html';

    private $type;
    private $content;

    /**
     * Gets body type
     *
     * @return string The body type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Sets body type
     *
     * @param  string The body type
     *
     * @return Body The body
     *
     * @throws  ConfigurationException if the type is not supported
     */
    public function setType($type)
    {
        switch ($type) {
            case self::TXT:
            case self::JSON:
            case self::HTML:
                break;
            default:
                throw new ConfigurationException("Invalid body type $type.");
        }
        $this->type = $type;
        return $this;
    }

    /**
     * Gets body content
     *
     * @return string The body content
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Sets body content
     *
     * @param  string The body content
     *
     * @return Body The body
     */
    public function setContent($content)
    {
        $this->content = $content;
        return $this;
    }
}
