<?php

namespace Gorcyn\Deveraux;

class ResponseParser
{
    /**
     * Parses route response
     *
     * @param string $projectPath The project path
     * @param string $name The route name
     * @param array $responseConfiguration The response configuration
     *
     * @return Response The parsed response
     *
     * @throws ConfigurationException if body is invalid
     */
    public function parse($projectPath, $name, array $responseConfiguration)
    {
        $response = new Response();

        $this->parseCode($response, $name, $responseConfiguration);
        $this->parseHeaders($response, $name, $responseConfiguration);
        $this->parseBody($response, $projectPath, $name, $responseConfiguration);

        return $response;
    }

    /**
     * Parses code
     *
     * @param Response The parsed response
     * @param string $name The route name
     * @param array $responseConfiguration The response configuration
     *
     * @throws ConfigurationException if code is missing
     */
    private function parseCode(Response &$response, $name, array $responseConfiguration)
    {
        // Code is mandatory
        if (!array_key_exists('code', $responseConfiguration)) {
            throw new ConfigurationException("Route $name response code is missing.");
        }
        $response->setCode($responseConfiguration['code']);
    }

    /**
     * Parses headers
     *
     * @param Response The parsed response
     * @param string $name The route name
     * @param array $responseConfiguration The response configuration
     *
     * @throws ConfigurationException if headers are invalid
     */
    private function parseHeaders(Response $response, $name, array $responseConfiguration)
    {
        // Headers is not mandatory but should be a not empty array
        if (array_key_exists('headers', $responseConfiguration)) {
            if (!is_array($responseConfiguration['headers'])) {
                throw new ConfigurationException("Route $name response headers is not valid.");
            }
            if (empty($responseConfiguration['headers'])) {
                throw new ConfigurationException("Route $name response headers is empty.");
            }
            foreach ($responseConfiguration['headers'] as $name => $value) {
                $header = new Header();
                $header->setName($name);
                $header->setValue($value);
                $response->addHeader($header);
            }
        }
    }

    /**
     * Parses body
     *
     * @param Response The parsed response
     * @param string $projectPath The project path
     * @param string $name The route name
     * @param array $responseConfiguration The response configuration
     *
     * @throws ConfigurationException if body is invalid
     */
    private function parseBody(Response $response, $projectPath, $name, array $responseConfiguration)
    {
        // Body is not mandatory but its type and content is
        if (array_key_exists('body', $responseConfiguration)) {
            // Body type is mandatory
            if (!array_key_exists('type', $responseConfiguration['body'])) {
                throw new ConfigurationException("Route $name response body type is missing.");
            }
            // Body content is mandatory
            if (!array_key_exists('content', $responseConfiguration['body'])) {
                throw new ConfigurationException("Route $name response body content is missing.");
            }
            // Body content file must exists
            $responseBodyContentPath = $projectPath.'/response/'.$responseConfiguration['body']['content'];
            if (!file_exists($responseBodyContentPath)) {
                throw new ConfigurationException("Route $name response body content file is missing.");
            }
            try {
                $body = new Body();
                $body->setType($responseConfiguration['body']['type']);
                $body->setContent(file_get_contents($responseBodyContentPath));
                $response->setBody($body);
            } catch (ConfigurationException $exception) {
                throw new ConfigurationException("Route $name response body type is invalid.", 0, $exception);
            }
        }
    }
}
