<?php

namespace Gorcyn\Deveraux;

use Symfony\Component\HttpFoundation\Response as SfResponse;

class ResponseBuilder
{
    /**
     * Build response for a given configuration
     *
     * @param  Response $responseConfiguration The response configuration to build
     *
     * @return SfResponse The built response
     */
    public static function build(Response $responseConfiguration)
    {
        // Prepare response with its code
        $response = new SfResponse(null, $responseConfiguration->getCode());

        // Has a body?
        $body = $responseConfiguration->getBody();
        if ($body != null) {
            self::appendBody($response, $responseConfiguration->getCode(), $body);
        }

        // Has headers?
        $headersConfiguration = $responseConfiguration->getHeaders();
        if ($headersConfiguration != null) {
            self::appendHeaders($response, $headersConfiguration);
        }
        return $response;
    }

    /**
     * Appends body
     *
     * @param  SfResponse $response The built response
     * @param  int $code The status code
     * @param  Body $body The body configuration
     */
    private static function appendBody(SfResponse &$response, $code, Body $body)
    {
        $response = new SfResponse($body->getContent(), $code);
        switch ($body->getType()) {
            case 'json':
                $response->headers->set('Content-Type', 'application/json');
                break;
            case 'html':
                $response->headers->set('Content-Type', 'text/html; charset=utf-8');
                break;
        }
    }

    /**
     * Appends headers
     *
     * @param  SfResponse $response The built response
     * @param  array $headersConfiguration The headers configuration
     */
    private static function appendHeaders(SfResponse &$response, array $headersConfiguration)
    {
        foreach ($headersConfiguration as $headerConfiguration) {
            $response->headers->set($headerConfiguration->getName(), $headerConfiguration->getValue(), true);
        }
    }
}
