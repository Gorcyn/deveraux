<?php

namespace Gorcyn\Deveraux;

class Route
{
    private $projectPath;
    private $request;
    private $response;

    /**
     * Gets route request
     *
     * @return Request The route request
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * Gets route response
     *
     * @return Response The route response
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * Creates a route
     *
     * @param string $projectPath The path to the project
     * @param string $name The route name
     * @param array $routeConfiguration The route configuration
     *
     * @throws ConfigurationException if route is invalid
     */
    public function __construct($projectPath, $name, $routeConfiguration)
    {
        $this->projectPath = $projectPath;

        // Request is mandatory
        if (!array_key_exists('request', $routeConfiguration)) {
            throw new ConfigurationException("Route $name request is missing.");
        }
        // Response is mandatory
        if (!array_key_exists('response', $routeConfiguration)) {
            throw new ConfigurationException("Route $name response is missing.");
        }
        $this->loadRequest($name, $routeConfiguration['request']);
        $this->loadResponse($name, $routeConfiguration['response']);
    }

    /**
     * Loads request
     *
     * @param  string The route name
     * @param  array The request configuration
     *
     * @throws ConfigurationException if route request is invalid
     */
    private function loadRequest($name, $requestConfiguration)
    {
        $parser = new RequestParser();
        try {
            $this->request = $parser->parse($this->projectPath, $name, $requestConfiguration);
        } catch (ConfigurationException $exception) {
            throw new ConfigurationException("Route $name configuration is not valid.", 0, $exception);
        }
    }

    /**
     * Loads response
     *
     * @param  string The route name
     * @param  array The response configuration
     *
     * @throws ConfigurationException if route response is invalid
     */
    private function loadResponse($name, $responseConfiguration)
    {
        $parser = new ResponseParser();
        try {
            $this->response = $parser->parse($this->projectPath, $name, $responseConfiguration);
        } catch (ConfigurationException $exception) {
            throw new ConfigurationException("Route $name configuration is not valid.", 0, $exception);
        }
    }
}
