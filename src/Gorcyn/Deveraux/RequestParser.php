<?php

namespace Gorcyn\Deveraux;

class RequestParser
{
    /**
     * Parses route request
     *
     * @param string $projectPath The project path
     * @param string $name The route name
     * @param array $requestConfiguration The request configuration
     *
     * @return Request The parsed request
     *
     * @throws ConfigurationException if body is invalid
     */
    public function parse($projectPath, $name, array $requestConfiguration)
    {
        $request = new Request();

        $this->parsePath($request, $name, $requestConfiguration);
        $this->parseMethod($request, $name, $requestConfiguration);
        $this->parseQuery($request, $name, $requestConfiguration);
        $this->parseHeaders($request, $name, $requestConfiguration);
        $this->parseBody($request, $projectPath, $name, $requestConfiguration);

        return $request;
    }

    /**
     * Parses path
     *
     * @param Request The parsed request
     * @param string $name The route name
     * @param array $requestConfiguration The request configuration
     *
     * @throws ConfigurationException if path is missing
     */
    private function parsePath(Request &$request, $name, array $requestConfiguration)
    {
        // Path is mandatory
        if (!array_key_exists('path', $requestConfiguration)) {
            throw new ConfigurationException("Route $name request path is missing.");
        }
        $request->setPath($requestConfiguration['path']);
    }

    /**
     * Parses method
     *
     * @param Request The parsed request
     * @param string $name The route name
     * @param array $requestConfiguration The request configuration
     *
     * @throws ConfigurationException if method is missing or invalid
     */
    private function parseMethod(Request &$request, $name, array $requestConfiguration)
    {
        // Method is mandatory
        if (!array_key_exists('method', $requestConfiguration)) {
            throw new ConfigurationException("Route $name request method is missing.");
        }
        try {
            $request->setMethod($requestConfiguration['method']);
        } catch (ConfigurationException $exception) {
            throw new ConfigurationException("Route $name request method is invalid.", 0, $exception);
        }
    }

    /**
     * Parses query
     *
     * @param Request The parsed request
     * @param string $name The route name
     * @param array $requestConfiguration The request configuration
     *
     * @throws ConfigurationException if query is invalid
     */
    private function parseQuery(Request &$request, $name, array $requestConfiguration)
    {
        // Query is not mandatory but should be a not empty array
        if (array_key_exists('query', $requestConfiguration)) {
            if (!is_array($requestConfiguration['query'])) {
                throw new ConfigurationException("Route $name request query is not valid.");
            }
            if (empty($requestConfiguration['query'])) {
                throw new ConfigurationException("Route $name request query is empty.");
            }
            $request->setQuery($requestConfiguration['query']);
        }
    }

    /**
     * Parses headers
     *
     * @param Request The parsed request
     * @param string $name The route name
     * @param array $requestConfiguration The request configuration
     *
     * @throws ConfigurationException if headers are invalid
     */
    private function parseHeaders(Request &$request, $name, array $requestConfiguration)
    {
        // Headers is not mandatory but should be a not empty array
        if (array_key_exists('headers', $requestConfiguration)) {
            if (!is_array($requestConfiguration['headers'])) {
                throw new ConfigurationException("Route $name request headers is not valid.");
            }
            if (empty($requestConfiguration['headers'])) {
                throw new ConfigurationException("Route $name request headers is empty.");
            }
            foreach ($requestConfiguration['headers'] as $name => $value) {
                $header = new Header();
                $header->setName($name);
                $header->setValue($value);
                $request->addHeader($header);
            }
        }
    }

    /**
     * Parses body
     *
     * @param Request The parsed request
     * @param string $projectPath The project path
     * @param string $name The route name
     * @param array $requestConfiguration The request configuration
     *
     * @throws ConfigurationException if body is invalid
     */
    private function parseBody(Request &$request, $projectPath, $name, array $requestConfiguration)
    {
        // Body is not mandatory but its type and content is
        if (array_key_exists('body', $requestConfiguration)) {
            // Body type is mandatory
            if (!array_key_exists('type', $requestConfiguration['body'])) {
                throw new ConfigurationException("Route $name request body type is missing.");
            }
            // Body content is mandatory
            if (!array_key_exists('content', $requestConfiguration['body'])) {
                throw new ConfigurationException("Route $name request body content is missing.");
            }
            // Body content file must exists
            $requestBodyContentPath = $projectPath.'/request/'.$requestConfiguration['body']['content'];
            if (!file_exists($requestBodyContentPath)) {
                throw new ConfigurationException("Route $name request body content file is missing.");
            }
            try {
                $body = new Body();
                $body->setType($requestConfiguration['body']['type']);
                $body->setContent(file_get_contents($requestBodyContentPath));
                $request->setBody($body);
            } catch (ConfigurationException $exception) {
                throw new ConfigurationException("Route $name request body type is invalid.", 0, $exception);
            }
        }
    }
}
