<?php

namespace Gorcyn\Deveraux;

abstract class Message
{
    private $headers;
    private $body;

    /**
     * Gets message headers
     *
     * @return array The message headers
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * Adds header
     *
     * @param  Header The header
     *
     * @return Message The message
     */
    public function addHeader(Header $header)
    {
        if ($this->headers == null) {
            $this->headers = array();
        }
        $this->headers[] = $header;
        return $this;
    }

    /**
     * Gets message body
     *
     * @return Body The message body
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Sets message body
     *
     * @param  Body The message body
     *
     * @return Message The message
     */
    public function setBody(Body $body)
    {
        $this->body = $body;
        return $this;
    }
}
