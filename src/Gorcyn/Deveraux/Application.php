<?php

namespace Gorcyn\Deveraux;

use Symfony\Component\HttpFoundation\Request as SfRequest;
use Symfony\Component\HttpFoundation\Response as SfResponse;

use Silex\Application as BaseApplication;

use Gorcyn\Deveraux\ConfigurationException;
use Gorcyn\Deveraux\ConfigurationLoader;
use Gorcyn\Deveraux\ResponseHandler;

class Application extends BaseApplication
{
    private $projectsFolder;

    /**
     * Instantiates a new Application.
     *
     * @param string $projectsFolder The projects folder.
     */
    public function __construct($projectsFolder = null)
    {
        parent::__construct();

        // Use default projects path if not provided
        if ($projectsFolder == null) {
            $projectsFolder = __DIR__.'/../../../projects';
        }
        $this->projectsFolder = $projectsFolder;

        // Intercepting /projectName/request/route with every possible HTTP method
        $this->match('/{projectName}/{route}', function (SfRequest $request, $projectName, $route) {
            try {
                // Loading configuration
                $loader = new ConfigurationLoader();
                $loader->load($this->projectsFolder, $projectName);

                // Handling request
                $responseHandler = new ResponseHandler($loader->getConfigurations());
                return $responseHandler->handleResponse($request, $projectName, $route);

            } catch (ConfigurationLoaderException $exception) {
                $this->abort(500, $exception->getMessage());
            } catch (ConfigurationException $exception) {
                $this->abort(500, $exception->getMessage());
            } catch (Exception $exception) {
                $this->abort(404, $exception->getMessage());
            }
        })->assert("route", ".*");
    }
}
