<?php

namespace Gorcyn\Deveraux;

class Request extends Message
{
    const OPTIONS = 'OPTIONS';
    const GET = 'GET';
    const HEAD = 'HEAD';
    const POST = 'POST';
    const PUT = 'PUT';
    const DELETE = 'DELETE';
    const TRACE = 'TRACE';
    const CONNECT = 'CONNECT';

    private $path;
    private $method;
    private $query;

    /**
     * Gets request path
     *
     * @return string The request path
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Sets request path
     *
     * @param  string The request path
     *
     * @return Request The request
     */
    public function setPath($path)
    {
        $this->path = $path;
        return $this;
    }

    /**
     * Gets request method
     *
     * @return string The request method
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * Sets request method
     *
     * @param  string The request method
     *
     * @return Request The request
     *
     * @throws  ConfigurationException if the method is not supported
     */
    public function setMethod($method)
    {
        switch ($method) {
            case self::OPTIONS:
            case self::GET:
            case self::HEAD:
            case self::POST:
            case self::PUT:
            case self::DELETE:
            case self::TRACE:
            case self::CONNECT:
                break;
            default:
                throw new ConfigurationException("Invalid request method $method.");
        }
        $this->method = $method;
        return $this;
    }

    /**
     * Gets request query parameters
     *
     * @return array The request query parameters
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * Sets request query parameters
     *
     * @param  array The request query parameters
     *
     * @return Request The request
     */
    public function setQuery($query)
    {
        $this->query = $query;
        return $this;
    }
}
