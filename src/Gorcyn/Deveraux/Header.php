<?php

namespace Gorcyn\Deveraux;

class Header
{
    private $name;
    private $value;

    /**
     * Gets header name
     *
     * @return string The header name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets header name
     *
     * @param  string The header name
     *
     * @return Header The header
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }


    /**
     * Gets header value
     *
     * @return string The header value
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Sets header value
     *
     * @param  string The header value
     *
     * @return Header The header
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }
}
