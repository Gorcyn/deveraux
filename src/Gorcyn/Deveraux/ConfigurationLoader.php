<?php

namespace Gorcyn\Deveraux;

use Symfony\Component\Yaml\Yaml;

class ConfigurationLoader
{
    private $configurations = array();

    /**
     * Gets configurations
     *
     * @return array The configurations
     */
    public function getConfigurations()
    {
        return $this->configurations;
    }

    /**
     * Loads configurations
     *
     * @param  string The projects folder
     * @param  string The project name
     *
     * @throws ConfigurationException if projects folder is invalid
     */
    public function load($projectsFolder, $projectName)
    {
        // Projects folder is mandatory
        if (!file_exists($projectsFolder)) {
            throw new ConfigurationException("Projets folder $projectsFolder does not exist.");
        }
        // Projects folder must be a folder
        if (!is_dir($projectsFolder)) {
            throw new ConfigurationException("Projets folder $projectsFolder is not a directory.");
        }
        // Project folder is mandatory
        if (!file_exists("$projectsFolder/$projectName")) {
            throw new ConfigurationException("Projet folder $projectsFolder/$projectName does not exist.");
        }
        // Project folder must be a folder
        if (!is_dir($projectsFolder)) {
            throw new ConfigurationException("Projet folder $projectsFolder/$projectName is not a directory.");
        }
        // Validating project
        $this->validateProject($projectsFolder, $projectName);
    }

    /**
     * Validates project
     *
     * @param  string The projects folder
     * @param  string The project name
     *
     * @throws ConfigurationException if project configuration is invalid
     */
    private function validateProject($projectsFolder, $projectName)
    {
        $projectPath = "$projectsFolder/$projectName";
        $projectConfigurationPath = "$projectPath/config.yml";

        // Project configuration file is mandatory
        if (!file_exists($projectConfigurationPath)) {
            throw new ConfigurationException("Project $projectName does not contain "
                . "any config.yml configuration file.");
        }
        $projectConfiguration = Yaml::parse(file_get_contents($projectConfigurationPath));
        // Project configuration file must not be empty
        if (empty($projectConfiguration)) {
            throw new ConfigurationException("Project $projectName configuration file is empty.");
        }
        foreach ($projectConfiguration as $name => $routeConfiguration) {
            try {
                $route = new Route($projectPath, $name, $routeConfiguration);

                // Adding project into projects configurations
                if (!array_key_exists($projectName, $this->configurations)) {
                    $this->configurations[$projectName] = array();
                }
                // Adding route to project into projects configurations
                $this->configurations[$projectName][$name] = $route;
            } catch (ConfigurationException $exception) {
                throw new ConfigurationException("Project $projectName configuration is not valid.", 0, $exception);
            }
        }
    }
}
