<?php

namespace Gorcyn\Deveraux;

use Symfony\Component\HttpFoundation\Request as SfRequest;

class ResponseHandler
{
    private $configurations;

    /**
     * Instantiates a new Application.
     *
     * @param array $configurations The route configurations
     */
    public function __construct(array $configurations)
    {
        $this->configurations = $configurations;
    }

    /**
     * Handles response
     *
     * @param  SfRequest $request     The received request
     * @param  string    $projectName The project name
     * @param  string    $route       The route
     *
     * @return SfResponse             The prepared response
     */
    public function handleResponse(SfRequest $request, $projectName, $route)
    {
        if (!array_key_exists($projectName, $this->configurations)) {
            throw new ConfigurationException("$projectName does not exist.");
        }
        // Sending response if request is known
        $configuration = $this->configurations[$projectName];

        // Looking for received request
        $matchingConfiguration = $this->findMatchingConfiguration($request, $route, $configuration);
        if ($matchingConfiguration == null) {
            throw new ConfigurationException("Route $route for project $projectName not found");
        }
        $responseConfiguration = $matchingConfiguration->getResponse();
        return ResponseBuilder::build($responseConfiguration);
    }

    /**
     * Finds matching configuration
     *
     * @param  SfRequest $request       The received request
     * @param  string    $route         The route
     * @param  array     $configuration The project configuration
     *
     * @return Route                    The matching route or null
     */
    private function findMatchingConfiguration(SfRequest $request, $route, array $configuration)
    {
        foreach ($configuration as $routeConfiguration) {
            $requestConfiguration = $routeConfiguration->getRequest();
            // Is same path?
            if ($requestConfiguration->getPath() != $route) {
                continue;
            }
            // Is same method?
            if ($requestConfiguration->getMethod() != $request->getMethod()) {
                continue;
            }
            // Has required query?
            $queriesConfiguration = $requestConfiguration->getQuery();
            if ($queriesConfiguration != null) {
                if ($this->doQueriesMatch($request, $queriesConfiguration) === false) {
                    continue;
                }
            }
            // Has required headers?
            $headersConfiguration = $requestConfiguration->getHeaders();
            if ($headersConfiguration != null) {
                if ($this->doHeadersMatch($request, $headersConfiguration) === false) {
                    continue;
                }
            }
            return $routeConfiguration;
        }
        return null;
    }

    /**
     * Checks if defined headers were sent
     *
     * @param  SfRequest $request              The received request
     * @param  array     $queriesConfiguration The configured headers
     *
     * @return boolean                         True if all configured headers have been received or false
     */
    private function doQueriesMatch(SfRequest $request, array $queriesConfiguration)
    {
        foreach ($queriesConfiguration as $key => $value) {
            if (!$request->query->has($key)) {
                return false;
            }
            if ($request->query->get($key) != $value) {
                return false;
            }
        }
        return true;
    }

    /**
     * Checks if defined headers were sent
     *
     * @param  SfRequest $request              The received request
     * @param  array     $headersConfiguration The configured query parameters
     *
     * @return boolean                         True if all configured query parameters have been received or false
     */
    private function doHeadersMatch(SfRequest $request, array $headersConfiguration)
    {
        foreach ($headersConfiguration as $headerConfiguration) {
            if (!$request->headers->has($headerConfiguration->getName())) {
                return false;
            }
            if ($request->headers->get($headerConfiguration->getName()) != $headerConfiguration->getValue()) {
                return false;
            }
        }
        return true;
    }
}
