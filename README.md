# Devereaux

## Principle

The main goal of this project is to deliver a fake server.

Base on a configuration file, this fake server is able to deliver specified responses to specified requests.

## Create project

To create a project just a folder with its name in the projects directory then add a config.yml in it. You can right now add _request_ and _response_ folders to this project.

## Add a request/response

To add a request/response specification, just give it a pretty funky name.

This element will then have a _request_ and a _response_.

The _request_ must follow the following specification:

* The _request_ **must** specify a _path_ and a _method_.
* The _path_ specifies the URL the application will consume.
* The _method_ can be one of all specified HTTP methods.
* The _request_ can contain _headers_ as key/value pairs.
* The _request_ can specify a _query_ matching the URL query string as key/value pairs.
* The _request_ can specify a _body_ part.
  * If it does, this _body_ **must** have a _type_ and a _content_.
  * Supported _body_ _types_ are _txt_, _json_ and _html_, and _xml_ sucks.
  * The _body_ _content_ is the path to the file containing the expected request content.

The _response_ must follow the following specification:

* The _response_ **must** specify a code.
* The _response_ can contain _headers_ as key/value pairs.
* The _response_ can contain a _body_ part.
  * If it does, this _body_ **must** have a _type_ and a _content_.
  * Supported _body_ _types_ are _txt_, _json_ and _html_, and _xml_ still sucks.
  * The _body_ _content_ is the path to the file containing the expected response content.

Sample:

    login:
      request:
        path: user/login
        method: POST
        headers:
          X-App-Name: MyAwesomeApp
        query:
          attempt: 1
        body:
          type: txt
          content: user_login.txt
      response:
        code: 200
        headers:
          Content-Language: en
          User-Token: THISISARANDOMLYGENERATEDUSERTOKEN
        body:
          type: json
          content: user_login.json

where *request/user_login.txt* is:

    login=gorcyn%40gmail.com&password=pWd

and *response/user_login.json* is:

    {
        "user": {
            "email": "gorcyn@gmail.com",
            "userName": "Gorcyn",
            "firstName": "Ludovic",
            "lastName": "Garcia"
        }
    }

## Run tests

Tests are implemented using [Behat][1].

### All projects

To tests all projects at once use:

    ./bin/behat

### Single project

To test one single project:

    ./bin/behat $PROJECT



[1]: http://behat.org/